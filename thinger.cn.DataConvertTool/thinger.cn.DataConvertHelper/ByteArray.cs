using System;
using System.Collections.Generic;
using System.Text;


// ***********************************************************************
//    Assembly       : 新阁教育
//    Created          : 2020-11-11
// ***********************************************************************
//     Copyright by 新阁教育（天津星阁教育科技有限公司）
//     QQ：        2934008828（付老师）  
//     WeChat：thinger002（付老师）
//     公众号：   dotNet工控上位机
//     哔哩哔哩：dotNet工控上位机
//     知乎：      dotNet工控上位机
//     头条：      dotNet工控上位机
//     视频号：   dotNet工控上位机
//     版权所有，严禁传播
// ***********************************************************************


namespace thinger.cn.DataConvertHelper
{
    /// <summary>
    /// 字节集合类
    /// </summary>
    public class ByteArray
    {

        #region 初始化

        private List<byte> list = new List<byte>();

        #endregion

        #region 获取字节数组

        /// <summary>
        /// 属性，返回字节数组
        /// </summary>
        public byte[] array
        {
            get { return list.ToArray(); }
        }
        #endregion

        #region 相关方法
        /// <summary>
        /// 清空字节数组
        /// </summary>
        public void Clear()
        {
            list = new List<byte>();
        }

        /// <summary>
        /// 添加一个字节
        /// </summary>
        /// <param name="item">字节</param>
        public void Add(byte item)
        {
            list.Add(item);
        }

        /// <summary>
        /// 添加一个字节数组
        /// </summary>
        /// <param name="items">字节数组</param>
        public void Add(byte[] items)
        {
            list.AddRange(items);
        }

        /// <summary>
        /// 添加一个ByteArray对象
        /// </summary>
        /// <param name="byteArray">ByteArray对象</param>
        public void Add(ByteArray byteArray)
        {
            list.AddRange(byteArray.array);
        }

        #endregion

    }
}
