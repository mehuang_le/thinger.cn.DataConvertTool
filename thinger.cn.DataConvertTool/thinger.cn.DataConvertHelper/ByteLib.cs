using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


// ***********************************************************************
//    Assembly       : 新阁教育
//    Created          : 2020-11-11
// ***********************************************************************
//     Copyright by 新阁教育（天津星阁教育科技有限公司）
//     QQ：        2934008828（付老师）  
//     WeChat：thinger002（付老师）
//     公众号：   dotNet工控上位机
//     哔哩哔哩：dotNet工控上位机
//     知乎：      dotNet工控上位机
//     头条：      dotNet工控上位机
//     视频号：   dotNet工控上位机
//     版权所有，严禁传播
// ***********************************************************************


namespace thinger.cn.DataConvertHelper
{
    /// <summary>
    /// 单个字节转换库
    /// </summary>
    public class ByteLib
    {

        #region 截取某个字节
        /// <summary>
        /// 从字节数组中截取某个字节
        /// </summary>
        /// <param name="source"></param>
        /// <param name="start"></param>
        /// <returns></returns>
        public static byte GetByteFromByteArray(byte[] source, int start)
        {
            byte[] b = ByteArrayLib.GetByteArray(source, start, 1);

            return b == null ? (byte)0 : b[0];
        }
        #endregion

        #region 将字节中某个位赋值
        /// <summary>
        /// 将字节中的某个位赋值
        /// </summary>
        /// <param name="value">原始字节</param>
        /// <param name="bit">位</param>
        /// <param name="val">写入数值</param>
        /// <returns>返回字节</returns>
        public static byte SetbitValue(byte value, int bit, bool val)
        {
            return val ? (byte)(value | (byte)Math.Pow(2, bit)) : (byte)(value & (byte)~(byte)Math.Pow(2, bit));
        }
        #endregion

    }
}
